/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.beast.operations

import edu.ucr.cs.bdlab.beast.common.{BeastOptions, CLIOperation}
import edu.ucr.cs.bdlab.beast.geolite._
import edu.ucr.cs.bdlab.beast.io.{FeatureWriter, SpatialOutputFormat, SpatialWriter}
import edu.ucr.cs.bdlab.beast.util.{CounterOutputStream, OperationMetadata}
import org.apache.commons.logging.{Log, LogFactory}
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.locationtech.jts.geom.impl.PackedCoordinateSequenceFactory
import org.locationtech.jts.geom.{Geometry, GeometryFactory}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.util.Random

@OperationMetadata(
  shortName =  "generate",
  description = "Generate geospatial objects with given distribution",
  inputArity = "0",
  outputArity = "1",
  inheritParams = Array(classOf[SpatialOutputFormat], classOf[DataGeneratorParams])
)
object DataGenerator extends CLIOperation {
  @transient lazy val logger:Log = LogFactory.getLog(getClass)

  lazy val factory: GeometryFactory = GeometryReader.DefaultInstance.getGeometryFactory

  /**
    * Distributions for generated data
    */
  object DistributionType extends Enumeration {
    /** Uniform distribution */
    val Uniform = Value("uniform")
    /** Diagonal distribution */
    val Diagonal = Value("dia")
    /** Parcel distribution */
    val Parcel = Value("parcel")
    /** Gaussian distribution */
    val Gaussian = Value("gaussian")
  }

  private def getGeometryType(strType: String, defaultValue: GeometryType): GeometryType = {
    strType match {
      case "point" => GeometryType.POINT
      case "envelope" => GeometryType.ENVELOPE
      case _ => defaultValue
    }
  }


  /**
    * Generates a uniform geometry with the given parameters
    * @param random
    * @param geom the geometry to modify
    * @param upperBound
    */
  private def makeUniformGeometry(random:Random, geom: Geometry, upperBound: Double): Unit = {
    geom.getGeometryType match {
      case "Point" => {
        val point: PointND = geom.asInstanceOf[PointND]
        for ($d <- 0 until point.getCoordinateDimension)
          point.setCoordinate($d, random.nextDouble)
      }
      case "Envelope" => {
        val envelope: EnvelopeND = geom.asInstanceOf[EnvelopeND]
        for ($d <- 0 until envelope.getCoordinateDimension) {
          envelope.setMinCoord($d, random.nextDouble)
          envelope.setMaxCoord($d, envelope.getMinCoord($d) + random.nextDouble * upperBound)
        }
      }
      case _ => throw new RuntimeException(s"Unsupported geometry type '${geom.getGeometryType()}'")
    }
  }

  private def makeDiagonalGeometry(random:Random, geom: Geometry, upperBound: Double,
                                   diagonalWidth: Double): Unit = {
    geom.getGeometryType match {
      case "Point" => {
        val point: PointND = geom.asInstanceOf[PointND]
        point.setCoordinate(0, random.nextDouble)
        for ($d <- 1 until point.getCoordinateDimension)
          point.setCoordinate($d, point.getCoordinate(0) + random.nextDouble * diagonalWidth)
      }
      case "Envelope" => {
        val envelope: EnvelopeND = geom.asInstanceOf[EnvelopeND]
        envelope.setMinCoord(0, random.nextDouble)
        envelope.setMaxCoord(0, envelope.getMinCoord(0) + random.nextDouble * upperBound)
        for ($d <- 1 until envelope.getCoordinateDimension) {
          envelope.setMinCoord($d, envelope.getMinCoord(0) + random.nextDouble * diagonalWidth)
          envelope.setMaxCoord($d, envelope.getMinCoord($d) + random.nextDouble * upperBound)
        }
      }
      case _ => throw new RuntimeException(s"Unsupported geometry type '${geom.getGeometryType()}'")
    }
  }


  /**
    * Splits the given envelope into two envelopes at a random point
    * @param envelope
    * @return
    */
  private def randomSplit(random: Random, envelope: EnvelopeNDLite): Array[EnvelopeNDLite] = {
    // Split the envelope into 2 envelopes
    def splitEnvelopes: Array[EnvelopeNDLite] = Array.ofDim[EnvelopeNDLite](2)
    val nDims: Int = envelope.getCoordinateDimension()

    val tilingRange: Double = 0.6
    val minSplitValueRatio: Double = (1 -tilingRange) / 2
    val maxSplitValueRatio: Double = 1 - minSplitValueRatio
    val randomSplitRatio: Double = minSplitValueRatio + (maxSplitValueRatio - minSplitValueRatio) * random.nextDouble()

    // Randomly choose the axis to split
    var axis: Int = random.nextInt(nDims)
    if(nDims == 2) {
      if((envelope.getMaxCoord(0) - envelope.getMinCoord(0)) > (envelope.getMaxCoord(1) - envelope.getMinCoord(1)))
        axis = 0
      else
        axis = 1
    }
    val splitValue: Double = envelope.getMinCoord(axis) + (envelope.getMaxCoord(axis) - envelope.getMinCoord(axis)) * randomSplitRatio

    splitEnvelopes(0) = new EnvelopeNDLite(envelope)
    splitEnvelopes(0).setMinCoord(axis, splitValue)
    splitEnvelopes(1) = new EnvelopeNDLite(envelope)
    splitEnvelopes(1).setMaxCoord(axis, splitValue)

    splitEnvelopes
  }


  /**
    * Generates splits using the parcel distribution to further process each part
    * @param random the random number generator
    * @param nDims number of dimensions
    * @param numSplits number of splits to generate
    * @return a list of envelopes for the generated splits
    */
  private def generateParcelSplits(random: Random, nDims: Int, numSplits: Int): List[EnvelopeNDLite] = {
    val minCoord: Array[Double] = Array.ofDim[Double](nDims)
    val maxCoord: Array[Double] = Array.ofDim[Double](nDims)
    for (i <- minCoord.indices) {
      minCoord(i) = 0.0
      maxCoord(i) = 1.0
    }
    val rootEnvelope: EnvelopeNDLite = new EnvelopeNDLite(minCoord, maxCoord)
    val queue: mutable.Queue[EnvelopeNDLite] = new mutable.Queue[EnvelopeNDLite]
    queue.enqueue(rootEnvelope)
    while (queue.length < numSplits) {
      val envelope: EnvelopeNDLite = queue.dequeue
      val splitEnvelopes: Array[EnvelopeNDLite] = randomSplit(random, envelope)
      for (i <- splitEnvelopes.indices)
        queue.enqueue(splitEnvelopes(i))
    }
    queue.toList
  }

  /**
    * Generates an RDD of geometries according to the given user parameters.
    * @param opts the generation user parameters
    * @param sc the spark context for creating the RDD
    * @return
    */
  def generateGeometries(opts: BeastOptions, sc: SparkContext): RDD[Geometry] = {
    // Extract index parameters from the command line arguments
    val size: Long = opts.getSizeAsBytes(DataGeneratorParams.Size, 1024L * 1024 * 1024)
    val distribution: DistributionType.Value = DistributionType.withName(opts.getString(DataGeneratorParams.Distribution, "uniform"))
    val numDimensions: Int = opts.getInt(DataGeneratorParams.Dimensions, 2)
    val geometryType = getGeometryType(opts.getString(DataGeneratorParams.Type), GeometryType.POINT)
    val upperBound: Double = opts.getDouble(DataGeneratorParams.UpperBound, 0.001)
    val diagonalWidth: Double = opts.getDouble(DataGeneratorParams.DiagonalWidth, 0.01)
    val seed: Long = opts.getLong(DataGeneratorParams.RandomSeed, Random.nextLong)
    val random: Random = new Random(seed)
    distribution match {
      case DistributionType.Parcel => generateParcel(sc, size, numDimensions, upperBound, seed, random, opts)
      case DistributionType.Uniform | DistributionType.Diagonal =>
        generateUniformDiagonal(sc, size, distribution, numDimensions, geometryType, upperBound, diagonalWidth, seed, opts)
      case other => throw new RuntimeException(s"Unsupported distributed type '$other'")
    }
  }

  /**
    * Generates uniformly distributed or diagonal distribute data
    * @param sc
    * @param size
    * @param distribution
    * @param numDimensions
    * @param geometryType
    * @param upperBound
    * @param diagonalWidth
    * @param seed
    * @param conf
    * @return
    */
  def generateUniformDiagonal(sc: SparkContext, size: Long, distribution: DistributionType.Value,
                              numDimensions: Int, geometryType: GeometryType, upperBound: Double,
                              diagonalWidth: Double, seed: Long, conf: BeastOptions): RDD[Geometry] = {
    // Generate random of splits so that each split will generate around 128 MB of data
    val numSplits: Int = Math.max(1, (size / (128L * 1024 * 1024 * 1024)).toInt)
    val featureWriterClass: Class[_ <: FeatureWriter] = SpatialOutputFormat.getConfiguredFeatureWriterClass(conf.loadIntoHadoopConf(null))
    val opts = new BeastOptions(conf) // For serialization
    val splitSize = size / numSplits
    sc.parallelize(1 to numSplits, numSplits).flatMap(i => {
      val featureWriter = featureWriterClass.newInstance
      featureWriter.initialize(new CounterOutputStream, opts.loadIntoHadoopConf(null))
      val localRandom: Random = new Random(seed + i)
      val geometries: ListBuffer[Geometry] = new ListBuffer[Geometry]
      var totalSplitSize: Long = 0
      val feature: Feature = new Feature
      while(totalSplitSize < splitSize) {
        val geometry: Geometry = geometryType match {
          case GeometryType.POINT => new PointND(factory, numDimensions)
          case GeometryType.ENVELOPE => new EnvelopeND(factory, numDimensions)
          case other => throw new RuntimeException(s"Unsupported geometry type '$other'")
        }
        distribution match {
          case DistributionType.Uniform => makeUniformGeometry(localRandom, geometry, upperBound)
          case DistributionType.Diagonal => makeDiagonalGeometry(localRandom, geometry, upperBound, diagonalWidth)
          case other => throw new RuntimeException(s"Unsupported distributed type '$other'")
        }
        feature.setGeometry(geometry)
        totalSplitSize += featureWriter.estimateSize(feature)
        geometries += geometry
      }
      geometries
    })
  }

  def generateParcel(sc: SparkContext, size: Long, numDimensions: Int, upperBound: Double, seed: Long, random: Random,
                     conf: BeastOptions): RDD[Geometry] = {
    // Use the configured feature writer class to estimate the output size
    val featureWriterClass: Class[_ <: FeatureWriter] = SpatialOutputFormat.getConfiguredFeatureWriterClass(conf.loadIntoHadoopConf(null))
    val opts = new BeastOptions(conf) // For serialization
    // Generate random of splits so that each split will generate around 128 MB of data
    val numSplits: Int = Math.max(1, (size / (128L * 1024 * 1024 * 1024)).toInt)
    val splitEnvelopes: List[EnvelopeNDLite] = generateParcelSplits(random, numDimensions, numSplits)
    val splitSize: Long = size / numSplits
    sc.parallelize(splitEnvelopes).flatMap(envelope => {
      // Initialize the feature writer that we will use to estimate the size
      val featureWriter = featureWriterClass.newInstance
      featureWriter.initialize(new CounterOutputStream, opts.loadIntoHadoopConf(null))
      // Create a feature that we will use to pass to the feature writer
      val feature: Feature = new Feature
      val localRandom = new Random(seed)
      val geometries: mutable.Queue[EnvelopeNDLite] = new mutable.Queue[EnvelopeNDLite]
      geometries.enqueue(envelope)
      var totalSplitSize = 0

      while (totalSplitSize < splitSize) {
        val e: EnvelopeNDLite = geometries.dequeue()
        val twoEnvelopes: Array[EnvelopeNDLite] = randomSplit(localRandom, e)
        for (oneEnvelope <- twoEnvelopes) {
          geometries.enqueue(oneEnvelope)
          feature.setGeometry(new EnvelopeND(factory, oneEnvelope))
          totalSplitSize += featureWriter.estimateSize(feature)
        }
      }

      // Dither all envelopes
      val density: Double = 1.0
      geometries.foreach(e => {
        val width = e.getSideLength(0) * density
        for ($d <- 0 until numDimensions) {
          e.setMaxCoord($d, e.getMinCoord($d) + (e.getMaxCoord($d) - e.getMinCoord($d)) * density)
          e.setMaxCoord($d, e.getMaxCoord($d) + width * upperBound * random.nextDouble())
        }
      })
      geometries.map(e => new EnvelopeND(factory, e))
    })
  }

  /**
    * Run the main function using the given user command-line options and spark context
    *
    * @param opts
    * @param sc
    * @return
    */
  override def run(opts: BeastOptions, inputs: Array[String], outputs: Array[String], sc: SparkContext): Any = {
    val generatedGeoms:RDD[Geometry] = generateGeometries(opts, sc)
    // Convert geometries to features
    val generatedFeatures:RDD[IFeature] = generatedGeoms.map(g => new Feature(g))
    SpatialWriter.saveFeatures(generatedFeatures, opts.getString(SpatialOutputFormat.OutputFormat), outputs(0), opts)
  }
}
