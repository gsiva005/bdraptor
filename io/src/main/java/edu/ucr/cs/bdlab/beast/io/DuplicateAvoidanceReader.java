package edu.ucr.cs.bdlab.beast.io;

import edu.ucr.cs.bdlab.beast.geolite.EnvelopeND;
import edu.ucr.cs.bdlab.beast.geolite.EnvelopeNDLite;
import edu.ucr.cs.bdlab.beast.geolite.IFeature;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import java.io.IOException;
import java.util.Iterator;

/**
 * A wrapper class around any {@link FeatureReader} to avoid duplicate records.
 * This ensures that duplicate records are not reported twice when the data is partitioned using
 * a disjoint (replicate) index. This class uses the reference point technique which assigns a rectangle to the
 * reader (region of interest) and only reports the records that have a top-left corner (reference point) inside the
 * region of interest.
 */
public class DuplicateAvoidanceReader extends FeatureReader {

  /**The underlying feature reader*/
  final protected FeatureReader wrapped;

  /**The region in which the reference point should lie*/
  final protected EnvelopeNDLite areaOfInterest = new EnvelopeNDLite();

  /**The search range to compute the reference point correctly*/
  final protected EnvelopeNDLite filterMBR = new EnvelopeNDLite();

  public DuplicateAvoidanceReader(FeatureReader _wrapped) {
    this.wrapped = _wrapped;
  }

  public void setAreaOfInterest(EnvelopeNDLite aoi) {
    this.areaOfInterest.setCoordinateDimension(aoi.getCoordinateDimension());
    this.areaOfInterest.set(aoi);
  }

  @Override
  public Iterator<IFeature> iterator() {
    return wrapped.iterator();
  }

  @Override
  public void initialize(InputSplit inputSplit, TaskAttemptContext context) throws IOException, InterruptedException {
    wrapped.initialize(inputSplit, context);
    Configuration conf = context.getConfiguration();
    this.areaOfInterest.set(((SpatialFileSplit)inputSplit).getEnvelope());
    // Retrieve filter MBR if exists
    if (conf.get(SpatialInputFormat.FilterMBR) != null) {
      EnvelopeNDLite.decodeString(conf.get(SpatialInputFormat.FilterMBR), filterMBR);
    } else {
      filterMBR.setEmpty();
    }
  }

  @Override
  public boolean nextKeyValue() throws IOException, InterruptedException {
    do {
      if (!wrapped.nextKeyValue())
        return false;
      // Check for duplicate avoidance
      EnvelopeNDLite recordMBR = new EnvelopeNDLite();
      recordMBR.setEmpty();
      recordMBR.merge(wrapped.getCurrentValue().getGeometry());
      boolean match = true;
      if (filterMBR.isEmpty() && !recordMBR.isEmpty()) {
        // Check the top-left corner of the record against the areaOfInterest
        for (int $d = 0; match && $d < areaOfInterest.getCoordinateDimension(); $d++) {
          double coord = recordMBR.getMinCoord($d);
          match = coord >= areaOfInterest.getMinCoord($d) && coord < areaOfInterest.getMaxCoord($d);
        }
      } else if (!recordMBR.isEmpty()) {
        for (int $d = 0; match && $d < areaOfInterest.getCoordinateDimension(); $d++) {
          double coord = Math.max(recordMBR.getMinCoord($d), filterMBR.getMinCoord($d));
          match = coord >= areaOfInterest.getMinCoord($d) && coord < areaOfInterest.getMaxCoord($d);
        }
      }
      if (match)
        return true;
      // Skip to next record
    } while (true);
  }

  @Override
  public EnvelopeND getCurrentKey() throws IOException, InterruptedException {
    return wrapped.getCurrentKey();
  }

  @Override
  public IFeature getCurrentValue() throws IOException, InterruptedException {
    return wrapped.getCurrentValue();
  }

  @Override
  public float getProgress() throws IOException, InterruptedException {
    return wrapped.getProgress();
  }

  @Override
  public void close() throws IOException {
    wrapped.close();
  }

}
