/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.beast.indexing;

import edu.ucr.cs.bdlab.beast.geolite.EnvelopeND;
import edu.ucr.cs.bdlab.beast.geolite.EnvelopeNDLite;
import edu.ucr.cs.bdlab.beast.geolite.GeometryReader;
import edu.ucr.cs.bdlab.beast.geolite.IFeature;
import edu.ucr.cs.bdlab.beast.io.CSVFeatureReader;
import edu.ucr.cs.bdlab.beast.synopses.Summary;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.LineReader;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.impl.PackedCoordinateSequenceFactory;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.List;

/**
 * A class that holds the information about one partition in a global index
 */
public class PartitionInfo extends Summary implements IFeature {

  /**A unique ID for the partition*/
  int partitionId;

  /**The filename (not full path) that has the contents of the file.*/
  String filename;

  public PartitionInfo() {}

  public PartitionInfo(int partitionId, String filename, EnvelopeNDLite mbb) {
    this.partitionId = partitionId;
    this.filename = filename;
    super.set(mbb);
  }

  public PartitionInfo(Summary summary, int partitionId) {
    super(summary);
    this.partitionId = partitionId;
    this.filename = String.format("part-%d", partitionId);
  }

  public PartitionInfo(PartitionInfo other) {
    super(other);
    this.partitionId = other.partitionId;
    this.filename = other.filename;
  }

  public String getFilename() {
    return filename;
  }

  /**The CSV head line of the master file*/
  public static final String CSVHeader = String.join("\t", "ID", "File Name",
      "Record Count", "Data Size", "Geometry");

  public StringBuilder envelopeAsWKT(StringBuilder out) {
    if (this.getCoordinateDimension() != 2)
      throw new RuntimeException("WKT not supported for higher-dimension polygons");
    out.append("POLYGON((");
    out.append(this.getMinCoord(0));
    out.append(' ');
    out.append(this.getMinCoord(1));
    out.append(',');

    out.append(this.getMaxCoord(0));
    out.append(' ');
    out.append(this.getMinCoord(1));
    out.append(',');

    out.append(this.getMaxCoord(0));
    out.append(' ');
    out.append(this.getMaxCoord(1));
    out.append(',');

    out.append(this.getMinCoord(0));
    out.append(' ');
    out.append(this.getMaxCoord(1));
    out.append(',');

    out.append(this.getMinCoord(0));
    out.append(' ');
    out.append(this.getMinCoord(1));
    out.append(')'); // The ring
    out.append(')'); // The polygon
    return out;
  }
  
  @Override
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();

    stringBuilder.append(partitionId);
    stringBuilder.append('\t');

    stringBuilder.append('\'');
    stringBuilder.append(filename);
    stringBuilder.append('\'');
    stringBuilder.append('\t');

    stringBuilder.append(numFeatures());
    stringBuilder.append('\t');

    stringBuilder.append(size());
    stringBuilder.append('\t');

    if (getCoordinateDimension() == 2)
      envelopeAsWKT(stringBuilder);

    for (int d = 0; d < getCoordinateDimension(); d++) {
      stringBuilder.append('\t');
      stringBuilder.append(getMinCoord(d));
    }

    for (int d = 0; d < getCoordinateDimension(); d++) {
      stringBuilder.append('\t');
      stringBuilder.append(getMaxCoord(d));
    }
    return stringBuilder.toString();
  }

  @Override
  public void writeExternal(ObjectOutput out) throws IOException {
    super.writeExternal(out);
    out.writeInt(partitionId);
    out.writeUTF(filename);
  }

  @Override
  public void readExternal(ObjectInput in) throws IOException {
    super.readExternal(in);
    this.partitionId = in.readInt();
    this.filename = in.readUTF();
  }

  /**
   * Read partition information from a master file as an array of partitions. The master file is a TSV
   * (tab-separated values) with a header line {@link #CSVHeader} and one line per partition. This method is supposed
   * to be called only when preparing a job and the master file is supposed to be of a decent size. Therefore, this
   * method does not optimize much for the object instantiation or memory overhead.
   * @param fs the file system that contains the file
   * @param file the path to the file
   * @return the list of partitions inside the given file
   * @throws IOException if an error happens while reading the file
   */
  public static PartitionInfo[] readPartitions(FileSystem fs, Path file) throws IOException {
    String quotes = "\'\'\"\"";
    LineReader reader = null;
    try {
      List<PartitionInfo> partitions = new ArrayList<>();
      reader = new LineReader(fs.open(file));
      Text line = new Text();
      // Read header
      if (reader.readLine(line) == 0)
        throw new RuntimeException("Header not found!");
      if (!line.toString().startsWith(CSVHeader))
        throw new RuntimeException("Incorrect header");

      // read partition information
      while (reader.readLine(line) > 0) {
        PartitionInfo partition = new PartitionInfo();
        partition.partitionId = Integer.parseInt(CSVFeatureReader.deleteAttribute(line, '\t', 0, quotes)); // partition ID
        partition.filename = CSVFeatureReader.deleteAttribute(line, '\t', 0, quotes);
        partition.setNumFeatures(Long.parseLong(CSVFeatureReader.deleteAttribute(line, '\t', 0, quotes)));
        partition.setSize(Long.parseLong(CSVFeatureReader.deleteAttribute(line, '\t', 0, quotes)));
        // Column #4 contains a WKT geom which is not used. Just drop it
        CSVFeatureReader.deleteAttribute(line, '\t', 0, quotes);
        String[] strCoords = line.toString().split("\t");
        // Read the envelope
        int numDimensions = strCoords.length / 2;
        double[] minCoords = new double[numDimensions];
        double[] maxCoords = new double[numDimensions];
        for (int i = 0; i < numDimensions; i++) {
          minCoords[i] = Double.parseDouble(strCoords[i]);
          maxCoords[i] = Double.parseDouble(strCoords[numDimensions + i]);
        }
        partition.set(minCoords, maxCoords);
        partitions.add(partition);
        line.clear();
      }
      return partitions.toArray(new PartitionInfo[partitions.size()]);
    } finally {
      if (reader != null)
        reader.close();
    }

  }

  @Override
  public Geometry getGeometry() {
    // Return the envelope
    EnvelopeND envelope = new EnvelopeND(GeometryReader.DefaultInstance.getGeometryFactory(), this);
    return envelope;
  }

  @Override
  public FieldType getAttributeType(int i) {
    switch (i) {
      case 0: // Partition ID
        return FieldType.IntegerType;
      case 1: // File name
      case 4: // WKT geometry
        return FieldType.StringType;
      case 2: // Number of features
      case 3: // Size in bytes
        return FieldType.LongType;
      default: // Coordinates
        return FieldType.DoubleType;
    }
  }

  @Override
  public Object getAttributeValue(int i) {
    switch (i) {
      case 0:
        return partitionId;
      case 1:
        return filename;
      case 2:
        return numFeatures();
      case 3:
        return size();
      case 4:
        // WKT representation of the envelope;
        StringBuilder str = new StringBuilder();
        envelopeAsWKT(str);
        return str.toString();
      default:
        return null;
    }
  }

  @Override
  public Object getAttributeValue(String name) {
    // "Record Count", "Data Size", "Geometry");
    switch (name) {
      case "ID": return 0;
      case "File Name": return 1;
      case "Record Count": return 2;
      case "Data Size": return 3;
      case "Geometry": return 4;
      default: return -1;
    }
  }

  @Override
  public int getNumAttributes() {
    return 5;
  }

  @Override
  public String getAttributeName(int i) {
    switch (i) {
      case 0: return "ID";
      case 1: return "File Name";
      case 2: return "Record Count";
      case 3: return "Data Size";
      case 4: return "Geometry";
      default: return null;
    }
  }

  @Override
  public int getStorageSize() {
    // ID + file name + record count + data size + WKT geometry (approximate) + coordinates
    return 8 + filename.length() + 8 + 8 + 100 + this.getCoordinateDimension() * 8 * 2;
  }

  @Override
  public void write(DataOutput out) throws IOException {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public void setGeometry(Geometry geom) {
    throw new RuntimeException("PartitionInfo is immutable");
  }
}
