/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.spark.beast

import edu.ucr.cs.bdlab.beast.indexing.{CellPartitioner, PartitionInfo, SparkSpatialPartitioner}
import edu.ucr.cs.bdlab.beast.io.SpatialFileSplit
import edu.ucr.cs.bdlab.beast.geolite.IFeature
import org.apache.spark.rdd.{NewHadoopPartition, RDD}
import org.apache.spark.{OneToOneDependency, Partition, Partitioner, TaskContext}

/**
 * Defines a spatially partitioned RDD, one where each partition is defined by an bounding box.
 * This RDD is used after loading an input from a disk index. It puts back the spatial partitions in the RDD.
 * @param parentRDD the input RDD that has spatial partitions
 */
class SpatiallyPartitionedRDD(var parentRDD: RDD[IFeature])
  extends RDD[IFeature](parentRDD.sparkContext, Seq(new OneToOneDependency(parentRDD))) {
  override def compute(split: Partition, context: TaskContext): Iterator[IFeature] =
    parentRDD.compute(split, context)

  override protected def getPartitions: Array[Partition] = parentRDD.partitions

  @transient var counter: Int = 0
  @transient val cells: Array[PartitionInfo] = parentRDD.partitions.map(baseP => {
    val hadoopPartition: NewHadoopPartition = baseP.asInstanceOf[NewHadoopPartition]
    val filesplit: SpatialFileSplit = hadoopPartition.serializableHadoopSplit.value.asInstanceOf[SpatialFileSplit]
    val partition = new PartitionInfo(counter, filesplit.getPath.getName, filesplit.getEnvelope(null))
    counter += 1
    partition
  })

  override val partitioner: Option[Partitioner] = Some(new SparkSpatialPartitioner(new CellPartitioner(cells: _*)))

  override def clearDependencies(): Unit = {
    super.clearDependencies()
    parentRDD = null
  }
}

object SpatialRDDHelper {
  /**
   * If the given RDD is build on spatially partitioned data, it will return [SpatiallyPartitionedRDD] that
   * points to the same data. Otherwise, the same RDD is returned.
   * @param rdd the input RDD
   * @return either the same input rdd if it is not spatially partitioned, or a new RDD that is spatially partitioned
   */
  def makeRDDSpatiallyPartition(rdd: RDD[IFeature]): RDD[IFeature] = {
    if (rdd.getNumPartitions == 0)
      return rdd
    val firstPartition = rdd.partitions(0)
    if (!firstPartition.isInstanceOf[NewHadoopPartition])
      return rdd
    if (!firstPartition.asInstanceOf[NewHadoopPartition].serializableHadoopSplit.value.isInstanceOf[SpatialFileSplit])
      return rdd
    if (!firstPartition.asInstanceOf[NewHadoopPartition].serializableHadoopSplit.value.asInstanceOf[SpatialFileSplit].getEnvelope(null).isFinite)
      return rdd
    // If we reach this step, it means that the input RDD contains spatial partitions with defined envelopes
    new SpatiallyPartitionedRDD(rdd)
  }
}