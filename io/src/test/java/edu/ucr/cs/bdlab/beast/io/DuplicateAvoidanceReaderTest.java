/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.beast.io;

import edu.ucr.cs.bdlab.beast.geolite.EnvelopeND;
import edu.ucr.cs.bdlab.beast.geolite.EnvelopeNDLite;
import edu.ucr.cs.bdlab.beast.geolite.Feature;
import edu.ucr.cs.bdlab.beast.geolite.IFeature;
import edu.ucr.cs.bdlab.beast.geolite.PointND;
import edu.ucr.cs.bdlab.test.JavaSparkTest;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.TaskAttemptID;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.task.TaskAttemptContextImpl;
import org.locationtech.jts.geom.GeometryFactory;

import java.io.File;
import java.io.IOException;

public class DuplicateAvoidanceReaderTest extends JavaSparkTest {

  public void testDuplicateAvoidanceWithPoints() throws IOException, InterruptedException {
    FeatureReader wrapped = new FeatureReader() {
      int count = 0;
      @Override
      public void initialize(InputSplit inputSplit, TaskAttemptContext taskAttemptContext) {
      }

      @Override
      public boolean nextKeyValue() {
        return count++ < 1;
      }

      @Override
      public EnvelopeND getCurrentKey() {
        return new EnvelopeND(new GeometryFactory(), 2, 0.0, 2.0, 0.0, 2.0);
      }

      @Override
      public IFeature getCurrentValue() {
        return new Feature(new PointND(new GeometryFactory(), 0.0, 2.0));
      }

      @Override
      public float getProgress() {
        return 0;
      }

      @Override
      public void close() {
      }
    };
    DuplicateAvoidanceReader reader = new DuplicateAvoidanceReader(wrapped);
    reader.setAreaOfInterest(new EnvelopeNDLite(2, 0.0, 0.0, 3.0, 3.0));
    // Set the filterMBR manually because we don't call the initialize function (and there is no master file)
    reader.filterMBR.set(new double[] {0.0, 0.0}, new double[] {3.0, 3.0});
    int count = 0;
    while (reader.nextKeyValue())
      count++;
    assertEquals(1, count);
  }

  public void testSkipDuplicateAvoidanceWhenReadingMasterFile() throws IOException, InterruptedException {
    Path indexPath = new Path(scratchPath(), "test_index");
    copyDirectoryFromResources("/test_index", new File(indexPath.toString()));
    Path inputFile = new Path(indexPath, "_master.grid");
    Configuration conf = new Configuration();
    conf.set(SpatialInputFormat.InputFormat, "envelope(5)");
    conf.setBoolean(CSVFeatureReader.SkipHeader, true);
    SpatialInputFormat format = new SpatialInputFormat();
    SpatialInputFormat.getFeatureReaderClass("envelope(5)");
    long fileLength = inputFile.getFileSystem(conf).getFileStatus(inputFile).getLen();
    InputSplit fileSplit = new FileSplit(inputFile, 0, fileLength, new String[0]);
    FeatureReader reader = format.createRecordReader(fileSplit, new TaskAttemptContextImpl(conf, new TaskAttemptID()));
    try {
      reader.initialize(fileSplit, conf);
      int count = 0;
      for (IFeature f : reader)
        count++;
      assertEquals(2, count);
    } finally {
      reader.close();
    }
  }
}