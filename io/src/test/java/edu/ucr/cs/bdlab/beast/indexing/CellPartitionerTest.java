/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.beast.indexing;

import edu.ucr.cs.bdlab.beast.geolite.EnvelopeND;
import edu.ucr.cs.bdlab.beast.geolite.EnvelopeNDLite;
import edu.ucr.cs.bdlab.beast.util.IntArray;
import edu.ucr.cs.bdlab.test.JavaSparkTest;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.locationtech.jts.geom.GeometryFactory;

import java.io.IOException;
import java.io.PrintStream;

public class CellPartitionerTest extends JavaSparkTest {

  public void testOverlapPartition() throws IOException {
    String[] masterFile = readTextResource("/test.partitions");
    Path masterPath = new Path(scratchPath(), "tempindex/_master.grid");
    Configuration conf = new Configuration();
    FileSystem fs = masterPath.getFileSystem(conf);
    PrintStream printStream = new PrintStream(fs.create(masterPath));
    for (String l : masterFile)
      printStream.println(l);
    printStream.close();
    //List<Partition> partitions = MetadataUtil.getPartitions(masterPath);
    PartitionInfo[] cells = PartitionInfo.readPartitions(masterPath.getFileSystem(conf), masterPath);
    CellPartitioner p = new CellPartitioner(cells);
    int cellId = p.overlapPartition(new EnvelopeND(new GeometryFactory(), 2, -124.7306754,40.4658126,-124.7306754,40.4658126));
    assertEquals(42, cells[cellId].partitionId);
    cellId = p.overlapPartition(new EnvelopeND(new GeometryFactory(), 2, 10.0, 46.0, 10.0, 46.0));
    assertEquals(10, cells[cellId].partitionId);
  }

  public void testOverlapPartitions() {
    PartitionInfo[] cells = new PartitionInfo[3];
    cells[0] = new PartitionInfo(1, "part-1", new EnvelopeNDLite(2, 0, 0, 1, 1));
    cells[1] = new PartitionInfo(2, "part-2", new EnvelopeNDLite(2, 1, 1, 2, 2));
    cells[2] = new PartitionInfo(3, "part-3", new EnvelopeNDLite(2, 1, 0, 2, 1));
    SpatialPartitioner partitioner = new CellPartitioner(cells);
    IntArray matchedPartitions = new IntArray();
    partitioner.overlapPartitions(new EnvelopeNDLite(2, 0, 0, 2, 1), matchedPartitions);
    assertEquals(2, matchedPartitions.size());
    matchedPartitions.sort();
    assertEquals(0, matchedPartitions.get(0));
    assertEquals(2, matchedPartitions.get(1));
  }

  public void testOverlapPartitionsWithPoints() {
    PartitionInfo[] cells = new PartitionInfo[1];
    cells[0] = new PartitionInfo(1, "part-1", new EnvelopeNDLite(2, 0, 0, 1, 1));
    SpatialPartitioner partitioner = new CellPartitioner(cells);
    IntArray matchedPartitions = new IntArray();
    partitioner.overlapPartitions(new EnvelopeNDLite(2, 0, 0, 0, 0), matchedPartitions);
    assertEquals(1, matchedPartitions.size());
    assertEquals(0, matchedPartitions.get(0));
  }

  public void testIsDisjoint() {
    PartitionInfo[] cells = new PartitionInfo[3];
    cells[0] = new PartitionInfo(1, "part-1", new EnvelopeNDLite(2, 0, 0, 1, 1));
    cells[1] = new PartitionInfo(2, "part-2", new EnvelopeNDLite(2, 1, 1, 2, 2));
    cells[2] = new PartitionInfo(3, "part-3", new EnvelopeNDLite(2, 1, 0, 2, 1));
    SpatialPartitioner partitioner = new CellPartitioner(cells);
    assertTrue("Should be disjoint", partitioner.isDisjoint());
    cells[0] = new PartitionInfo(1, "part-1", new EnvelopeNDLite(2, 0, 0, 1, 1));
    cells[1] = new PartitionInfo(2, "part-2", new EnvelopeNDLite(2, 1, 1, 2, 2));
    cells[2] = new PartitionInfo(3, "part-3", new EnvelopeNDLite(2, 0.5, 0.5, 1.5, 1.5));
    partitioner = new CellPartitioner(cells);
    assertFalse("Should not be disjoint", partitioner.isDisjoint());
  }
}