/*
 * Copyright 2019 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.beast.io;

import edu.ucr.cs.bdlab.beast.common.BeastOptions;
import edu.ucr.cs.bdlab.beast.geolite.EnvelopeND;
import edu.ucr.cs.bdlab.beast.geolite.GeometryType;
import edu.ucr.cs.bdlab.beast.geolite.IFeature;
import edu.ucr.cs.bdlab.test.JavaSparkTest;
import edu.ucr.cs.bdlab.beast.util.OperationException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.spark.api.java.JavaPairRDD;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.Stack;

public class SpatialInputFormatTest extends JavaSparkTest {

  public void testDoNotSkipHiddenFilesIfExplicitlySpecified() {
    Path csvPath = new Path(scratchPath(), "_temp.csv");
    copyResource("/test_wkt.csv", new File(csvPath.toString()));
    Configuration conf = sparkContext().hadoopConfiguration();
    conf.set(SpatialInputFormat.InputFormat, "wkt(1)");
    conf.setBoolean(CSVFeatureReader.SkipHeader, true);
    conf.set(CSVFeatureReader.FieldSeparator, "\t");
    JavaPairRDD<EnvelopeND, IFeature> records = javaSparkContext().newAPIHadoopFile(csvPath.toString(), SpatialInputFormat.class, EnvelopeND.class, IFeature.class, conf);
    List<IFeature> features = records.values().collect();
    assertEquals(2, features.size());
  }

  public void testReadCSVFiles() {
    Path csvPath = new Path(scratchPath(), "temp.csv");
    copyResource("/test_wkt.csv", new File(csvPath.toString()));
    Configuration conf = sparkContext().hadoopConfiguration();
    conf.set(SpatialInputFormat.InputFormat, "wkt(1)");
    conf.setBoolean(CSVFeatureReader.SkipHeader, true);
    conf.set(CSVFeatureReader.FieldSeparator, "\t");
    JavaPairRDD<EnvelopeND, IFeature> records = javaSparkContext().newAPIHadoopFile(csvPath.toString(), SpatialInputFormat.class, EnvelopeND.class, IFeature.class, conf);
    List<IFeature> features = records.values().collect();
    assertEquals(2, features.size());
  }

  public void testDuplicateAvoidanceWithNoRangeQuery() {
    // Read a disjoint partitioned file and make sure that duplicate records are not repeated
    Path indexPath = new Path(scratchPath(), "temp");
    copyDirectoryFromResources("/test_index", new File(indexPath.toString()));
    Configuration conf = sparkContext().hadoopConfiguration();
    conf.set(SpatialInputFormat.InputFormat, "envelopek(2)");
    conf.set(CSVFeatureReader.FieldSeparator, ",");
    JavaPairRDD<EnvelopeND, IFeature> records = javaSparkContext().newAPIHadoopFile(indexPath.toString(), SpatialInputFormat.class, EnvelopeND.class, IFeature.class, conf);
    List<IFeature> features = records.values().collect();
    assertEquals(3, features.size());
  }

  public void testDuplicateAvoidanceWithRangeQuery() {
    // Read a disjoint partitioned file and make sure that duplicate records are not repeated
    Path indexPath = new Path(scratchPath(), "temp");
    copyDirectoryFromResources("/test_index", new File(indexPath.toString()));
    Configuration conf = sparkContext().hadoopConfiguration();
    conf.set(SpatialInputFormat.FilterMBR, "2,2,4,4");
    conf.set(SpatialInputFormat.InputFormat, "envelopek(2)");
    conf.set(CSVFeatureReader.FieldSeparator, ",");
    JavaPairRDD<EnvelopeND, IFeature> records = javaSparkContext().newAPIHadoopFile(indexPath.toString(), SpatialInputFormat.class, EnvelopeND.class, IFeature.class, conf);
    List<IFeature> features = records.values().collect();
    assertEquals(2, features.size());
  }

  public void testSkipDupAvoidance() {
    // Read a partitioned file with repeated records and make sure that the repeated record gets read twice
    Path indexPath = new Path(scratchPath(), "temp");
    copyDirectoryFromResources("/test_index", new File(indexPath.toString()));
    Configuration conf = sparkContext().hadoopConfiguration();
    conf.set(SpatialInputFormat.InputFormat, "envelopek(2)");
    conf.setBoolean(SpatialInputFormat.DuplicateAvoidance, false);
    conf.set(CSVFeatureReader.FieldSeparator, ",");
    JavaPairRDD<EnvelopeND, IFeature> records = javaSparkContext().newAPIHadoopFile(indexPath.toString(), SpatialInputFormat.class, EnvelopeND.class, IFeature.class, conf);
    List<IFeature> features = records.values().collect();
    assertEquals(4, features.size());
  }

  public void testRecursive() {
    Path filePath = new Path(scratchPath(), "dir");
    Path csvPath = new Path(filePath, "subdir/deeper/tree/temp.csv");
    copyResource("/test_points.csv", new File(csvPath.toString()));

    Configuration conf = sparkContext().hadoopConfiguration();
    conf.set(CSVFeatureReader.FieldSeparator, ",");
    conf.setBoolean(CSVFeatureReader.SkipHeader, true);
    conf.setBoolean(SpatialInputFormat.Recursive, true);
    conf.set(SpatialInputFormat.InputFormat, "point(1,2)");
    JavaPairRDD<EnvelopeND, IFeature> records = javaSparkContext().newAPIHadoopFile(filePath.toString(), SpatialInputFormat.class, EnvelopeND.class, IFeature.class, conf);
    List<IFeature> features = records.values().collect();
    assertEquals(2, features.size());
  }

  public void testRecursiveWithShapefile() throws IOException {
    Path filePath = new Path(scratchPath(), "dir");
    Path shpPath = new Path(filePath, "subdir/deeper/tree/");
    copyDirectoryFromResources("/usa-major-cities", new File(shpPath.toString()));

    Configuration conf = sparkContext().hadoopConfiguration();
    conf.setBoolean(SpatialInputFormat.Recursive, true);
    conf.set(SpatialInputFormat.InputFormat, "shapefile");
    SpatialInputFormat inputFormat = new SpatialInputFormat();
    Job job = Job.getInstance(conf);
    SpatialInputFormat.setInputPaths(job, filePath);
    List<InputSplit> splits = inputFormat.getSplits(job);
    assertEquals(1, splits.size());
    assertEquals("usa-major-cities.shp", ((FileSplit)splits.get(0)).getPath().getName());
  }
  public void testReadCSVPoints() {
    Path csvPath = new Path(scratchPath(), "temp.csv");
    copyResource("/test_points.csv", new File(csvPath.toString()));

    Configuration conf = sparkContext().hadoopConfiguration();
    conf.set(CSVFeatureReader.FieldSeparator, ",");
    conf.setBoolean(CSVFeatureReader.SkipHeader, true);
    conf.set(SpatialInputFormat.InputFormat, "point(1,2)");
    SpatialInputFormat.getFeatureReaderClass("point(1,2)");
    JavaPairRDD<EnvelopeND, IFeature> records = javaSparkContext().newAPIHadoopFile(csvPath.toString(), SpatialInputFormat.class, EnvelopeND.class, IFeature.class, conf);
    List<IFeature> features = records.values().collect();
    assertEquals(2, features.size());
  }

  public void testReadShapefiles() {
    Path shpPath = new Path(scratchPath(), "temp.shp");
    copyResource("/usa-major-cities/usa-major-cities.shp", new File(shpPath.toString()));
    Path dbfPath = new Path(scratchPath(), "temp.dbf");
    copyResource("/usa-major-cities/usa-major-cities.dbf", new File(dbfPath.toString()));

    Configuration conf = sparkContext().hadoopConfiguration();
    conf.set(SpatialInputFormat.InputFormat, "shapefile");
    SpatialInputFormat.getFeatureReaderClass("shapefile");
    JavaPairRDD<EnvelopeND, IFeature> records = javaSparkContext().newAPIHadoopFile(shpPath.toString(), SpatialInputFormat.class, EnvelopeND.class, IFeature.class, conf);
    List<IFeature> features = records.values().collect();
    assertEquals(120, features.size());
  }

  public void testReadShapefilesFromDirectory() throws IOException {
    FileSystem fs = scratchPath().getFileSystem(new Configuration());
    Path inputPath = new Path(scratchPath(), "temp");
    fs.mkdirs(inputPath);
    Path shpPath = new Path(inputPath, "temp.shp");
    copyResource("/usa-major-cities/usa-major-cities.shp", new File(shpPath.toString()));
    Path dbfPath = new Path(inputPath, "temp.dbf");
    copyResource("/usa-major-cities/usa-major-cities.dbf", new File(dbfPath.toString()));

    Configuration conf = sparkContext().hadoopConfiguration();
    conf.set(SpatialInputFormat.InputFormat, "shapefile");
    JavaPairRDD<EnvelopeND, IFeature> records = javaSparkContext().newAPIHadoopFile(inputPath.toString(), SpatialInputFormat.class, EnvelopeND.class, IFeature.class, conf);
    List<IFeature> features = records.values().collect();
    assertEquals(120, features.size());
  }

  public void testReadManyShapefilesFromDirectory() throws IOException {
    FileSystem fs = scratchPath().getFileSystem(new Configuration());
    Path inputPath = new Path(scratchPath(), "temp");
    fs.mkdirs(inputPath);
    copyResource("/usa-major-cities/usa-major-cities.shp", new File(new Path(inputPath, "temp.shp").toString()));
    copyResource("/usa-major-cities/usa-major-cities.dbf", new File(new Path(inputPath, "temp.dbf").toString()));
    copyResource("/usa-major-cities/usa-major-cities.shp", new File(new Path(inputPath, "temp2.shp").toString()));
    copyResource("/usa-major-cities/usa-major-cities.dbf", new File(new Path(inputPath, "temp2.dbf").toString()));

    Configuration conf = sparkContext().hadoopConfiguration();
    conf.set(SpatialInputFormat.InputFormat, "shapefile");
    JavaPairRDD<EnvelopeND, IFeature> records = javaSparkContext().newAPIHadoopFile(inputPath.toString(), SpatialInputFormat.class, EnvelopeND.class, IFeature.class, conf);
    List<IFeature> features = records.values().collect();
    assertEquals(120 * 2, features.size());
  }

  public void testShouldGenerateSplitsFromMasterFile() throws IOException {
    // Create a directory with a master file and some data files
    // Make sure that getSplits method will be limited to what is listed in the master file
    Configuration conf = new Configuration();
    conf.set(SpatialInputFormat.InputFormat, "point(0,1)");
    Path indexPath = new Path(scratchPath(), "index");
    FileSystem fileSystem = indexPath.getFileSystem(conf);
    fileSystem.mkdirs(indexPath);
    Path masterFilePath = new Path(indexPath, "_master.heap");
    copyResource("/test.partitions", new File(masterFilePath.toString()));
    // Create fake files
    for (int partitionID = 0; partitionID < 45; partitionID++) {
      fileSystem.create(new Path(indexPath, String.format("part-%05d", partitionID))).close();
    }
    Job job = Job.getInstance(conf);
    SpatialInputFormat.addInputPath(job, indexPath);
    SpatialInputFormat inputFormat = new SpatialInputFormat() {
      @Override
      protected boolean isSplitable(JobContext context, Path filename) {
        return false;
      }
    };
    List<InputSplit> status = inputFormat.getSplits(job);
    assertEquals(44, status.size());
  }

  public void testCustomBlockSize() throws IOException {
    File input = makeFileCopy("/test.partitions");
    SpatialInputFormat inputFormat = new SpatialInputFormat();
    Configuration conf = new Configuration();
    conf.setLong(SpatialInputFormat.MaxSplitSize, 6 * 1024);
    conf.set(SpatialInputFormat.InputFormat, "wkt");
    Job job = Job.getInstance(conf);
    SpatialInputFormat.addInputPath(job, new Path(input.getPath()));
    List<InputSplit> splits = inputFormat.getSplits(job);
    assertEquals(2, splits.size());
  }

  public void testShouldPruneNonMatchingFiles() throws IOException {
    // Create a master file with different MBRs for different files. Make sure the getSplits method returns only
    // the splits that overlap the query MBR
    // Create a directory with a master file and some data files
    // Make sure that getSplits method will be limited to what is listed in the master file
    Configuration conf = new Configuration();
    conf.set(SpatialInputFormat.FilterMBR, "-125,55,-55,60");
    conf.set(SpatialInputFormat.InputFormat, "point(0,1)");
    Path indexPath = new Path(scratchPath(), "index");
    FileSystem fileSystem = indexPath.getFileSystem(conf);
    fileSystem.mkdirs(indexPath);
    Path masterFilePath = new Path(indexPath, "_master.heap");
    copyResource("/test.partitions", new File(masterFilePath.toString()));
    // Create fake files
    for (int partitionID = 0; partitionID < 45; partitionID++) {
      fileSystem.create(new Path(indexPath, String.format("part-%05d", partitionID))).close();
    }
    Job job = Job.getInstance(conf);
    SpatialInputFormat.addInputPath(job, indexPath);
    SpatialInputFormat inputFormat = new SpatialInputFormat() {
      @Override
      protected boolean isSplitable(JobContext context, Path filename) {
        return false;
      }
    };
    List<InputSplit> status = inputFormat.getSplits(job);
    assertEquals(3, status.size());
  }

  public void testShouldWorkWithCompressedShapeFiles() throws IOException {
    // Point to an input directory that has several compressed zip files. Make sure that the records inside these
    // shapefiles will be returned.
    Path inputPath = new Path(scratchPath(), "index");
    Configuration conf = sparkContext().hadoopConfiguration();
    conf.set(SpatialInputFormat.InputFormat, "shapefile");
    SpatialInputFormat.getFeatureReaderClass("shapefile");
    inputPath.getFileSystem(conf).mkdirs(inputPath);
    copyResource("/usa-major-cities.zip", new File(new Path(inputPath, "temp1.zip").toString()));
    copyResource("/usa-major-cities.zip", new File(new Path(inputPath, "temp2.zip").toString()));

    JavaPairRDD<EnvelopeND, IFeature> values = javaSparkContext().newAPIHadoopFile(inputPath.toString(), SpatialInputFormat.class, EnvelopeND.class, IFeature.class, conf);
    List<IFeature> geometries = values.values().collect();
    assertEquals(240, geometries.size());
  }

  public void testShouldNotSplitZipFiles() throws IOException {
    Path inputPath = new Path(scratchPath(), "shape.zip");
    copyResource("/usa-major-cities.zip", new File(inputPath.toString()));

    Configuration conf = sparkContext().hadoopConfiguration();
    conf.set(SpatialInputFormat.InputFormat, "shapefile");
    conf.setLong(FileInputFormat.SPLIT_MAXSIZE, 2*1024);
    conf.setLong(FileInputFormat.SPLIT_MINSIZE, 1024);
    Job job = Job.getInstance(conf);
    SpatialInputFormat.addInputPath(job, inputPath);
    SpatialInputFormat inputFormat = new SpatialInputFormat();
    List<InputSplit> splits = inputFormat.getSplits(job);
    assertEquals(1, splits.size());
  }

  public void testShouldPruneHigherDimensional() throws IOException {
    // Create a master file with different MBRs for different files. Make sure the getSplits method returns only
    // the splits that overlap the query MBR
    // Create a directory with a master file and some data files
    // Make sure that getSplits method will be limited to what is listed in the master file
    Configuration conf = new Configuration();
    conf.set(SpatialInputFormat.FilterMBR, "3,2,11,5,3,12");
    conf.set(SpatialInputFormat.InputFormat, "pointk(3)");
    Path indexPath = new Path(scratchPath(), "index");
    Path masterFilePath = new Path(indexPath, "_master.heap");
    FileSystem fileSystem = indexPath.getFileSystem(conf);
    fileSystem.mkdirs(indexPath);
    PrintStream ps = new PrintStream(fileSystem.create(masterFilePath));
    ps.println("ID\tFile Name\tRecord Count\tData Size\tGeometry");
    ps.println("1\tpart-00000\t10\t100\t\t1\t1\t8\t4\t3\t9");
    ps.println("2\tpart-00001\t20\t200\t\t4\t2\t10\t6\t4\t12");
    ps.close();
    // Create fake files
    fileSystem.create(new Path(indexPath, "part-00000")).close();
    fileSystem.create(new Path(indexPath, "part-00001")).close();

    Job job = Job.getInstance(conf);
    SpatialInputFormat.addInputPath(job, indexPath);
    SpatialInputFormat inputFormat = new SpatialInputFormat() {
      @Override
      protected boolean isSplitable(JobContext context, Path filename) {
        return false;
      }
    };
    List<InputSplit> status = inputFormat.getSplits(job);
    assertEquals(1, status.size());
    assertEquals("part-00001", ((FileSplit)status.get(0)).getPath().getName());
  }


  public void testSetInputFormatShouldThrowAnExceptionForWrongFormats() {
    // A valid input should not raise an exception
    boolean errorRaised = false;
    try {
      BeastOptions opts = new BeastOptions();
      opts.set(SpatialInputFormat.InputFormat, "point(1,2)");
    } catch (OperationException e) {
      errorRaised = true;
    }
    assertFalse("No error should be raised", errorRaised);

    // An invalid input should raise an exception
    errorRaised = false;
    try {
      SpatialInputFormat.getFeatureReaderClass("errrr");
    } catch (OperationException e) {
      errorRaised = true;
    }
    assertTrue("An error should be raised", errorRaised);

    // A correction for slight typos should be reported in the error message
    errorRaised = false;
    try {
      SpatialInputFormat.getFeatureReaderClass("piont");
    } catch (OperationException e) {
      errorRaised = true;
      assertTrue("The error message should suggest a correction", e.getMessage().contains("point"));
    }
    assertTrue("An error should be raised", errorRaised);

    // Suggest a correction when extra parameters are added
    errorRaised = false;
    try {
      SpatialInputFormat.getFeatureReaderClass("piont(1,2)");
    } catch (OperationException e) {
      errorRaised = true;
      assertTrue("The error message should suggest a correction", e.getMessage().contains("point"));
    }
    assertTrue("An error should be raised", errorRaised);
  }

  public void testSetInputFormatShouldAutoDetectTheInput() {
    // A shapefile is detected from the file extension without opening the file
    String inputPath = "input.shp";
    BeastOptions detectedOptions;
    detectedOptions = SpatialInputFormat.autodetectInputFormat(new Configuration(), inputPath);
    assertNotNull(detectedOptions);
    assertEquals("shapefile", detectedOptions.get(SpatialInputFormat.InputFormat).get());

    // Test a case of unsupported file
    inputPath = "input.xyz";
    detectedOptions = SpatialInputFormat.autodetectInputFormat(new Configuration(), inputPath);
    assertNull("Should not be able to autodetect the input", detectedOptions);

    // Test auto detection when the input path points to a directory
    inputPath = new Path(scratchPath(), "testdir").toString();
    copyDirectoryFromResources("/usa-major-cities", new File(inputPath));
    detectedOptions = SpatialInputFormat.autodetectInputFormat(new Configuration(), inputPath);
    assertNotNull("Should be able to auto-detect the input", detectedOptions);
    assertEquals("shapefile", detectedOptions.get(SpatialInputFormat.InputFormat).get());
  }

  public void testAddDependentClassesWithMultipleInputFormats() {
    BeastOptions opts = new BeastOptions(false)
        .set("iformat[1]", "wkt")
        .set("iformat[0]", "geojson")
        .set("separator", ",");
    Stack<Class<?>> dependentClasses = new Stack<>();
    new SpatialInputFormat().addDependentClasses(opts, dependentClasses);
    assertEquals(2, dependentClasses.size());
  }

  public void testAddDependentClassesWithOneInputFormat() {
    BeastOptions opts = new BeastOptions(false).set("iformat", "wkt");
    Stack<Class<?>> dependentClasses = new Stack<>();
    new SpatialInputFormat().addDependentClasses(opts, dependentClasses);
    assertEquals(1, dependentClasses.size());
  }

  public void testReadOneFeature() throws IOException {
    // Test read from an indexed directory
    Path inPath = new Path(scratchPath(), "test_index");
    copyDirectoryFromResources("/test_index", new File(inPath.toString()));
    BeastOptions opts = new BeastOptions().set("iformat", "point").set("separator", ",");
    FileSystem fileSystem = inPath.getFileSystem(new Configuration());
    IFeature feature = SpatialInputFormat.readOne(inPath, opts.loadIntoHadoopConf(null));
    assertEquals(GeometryType.POINT.typename, feature.getGeometry().getGeometryType());
    assertEquals(2, feature.getNumAttributes());

    // Test read from a file
    feature = SpatialInputFormat.readOne(new Path(inPath, "part-00.csv"), opts.loadIntoHadoopConf(null));
    assertEquals(GeometryType.POINT.typename, feature.getGeometry().getGeometryType());
    assertEquals(2, feature.getNumAttributes());

    // Test read from a non-indexed directory
    fileSystem.delete(new Path(inPath, "_master.grid"), false);
    feature = SpatialInputFormat.readOne(inPath, opts.loadIntoHadoopConf(null));
    assertEquals(GeometryType.POINT.typename, feature.getGeometry().getGeometryType());
    assertEquals(2, feature.getNumAttributes());

    // Test read a directory recursively
    inPath = new Path(scratchPath(), "dir/subdir/test_index");
    fileSystem.mkdirs(inPath);
    copyDirectoryFromResources("/test_index", new File(inPath.toString()));
    opts = new BeastOptions().set("iformat", "point").set("separator", ",").setBoolean("recursive", true);
    feature = SpatialInputFormat.readOne(new Path(scratchPath(), "dir"), opts.loadIntoHadoopConf(null));
    assertEquals(GeometryType.POINT.typename, feature.getGeometry().getGeometryType());
    assertEquals(2, feature.getNumAttributes());
  }

  public void testGetFeatureReaderClass() {
    // 1- Test if the feature reader class is set
    Configuration conf = new Configuration();
    Class<? extends FeatureReader> expectedClass = CSVFeatureReader.class;
    conf.setClass(SpatialInputFormat.FeatureReaderClass, expectedClass, FeatureReader.class);
    Class<? extends FeatureReader> actualClass = SpatialInputFormat.getFeatureReaderClass(conf, new Path("."));
    assertEquals(expectedClass, actualClass);

    // 2- Test creating from iformat
    conf = new Configuration();
    conf.set(SpatialInputFormat.InputFormat, "wkt");
    actualClass = SpatialInputFormat.getFeatureReaderClass(conf, new Path("."));
    assertEquals(expectedClass, actualClass);
    // Assert that it was explicitly set for future calls
    actualClass = conf.getClass(SpatialInputFormat.FeatureReaderClass, null, FeatureReader.class);
    assertEquals(expectedClass, actualClass);

    // 3- Autodetect the input if not set
    conf = new Configuration();
    Path input = new Path(scratchPath(), "input.csv");
    copyResource("/test_points.csv", new File(input.toString()));
    actualClass = SpatialInputFormat.getFeatureReaderClass(conf, input);
    assertEquals(expectedClass, actualClass);
    // Assert that the configuration was updated
    assertEquals("point(1,2)", conf.get(SpatialInputFormat.InputFormat));
    assertEquals(",", conf.get(CSVFeatureReader.FieldSeparator));
    assertTrue("skip header should be set", conf.getBoolean(CSVFeatureReader.SkipHeader, false));
    // Assert that it was explicitly set for future calls
    actualClass = conf.getClass(SpatialInputFormat.FeatureReaderClass, null, FeatureReader.class);
    assertEquals(expectedClass, actualClass);
  }

  public void testReadMasterFileWithCustomQuotes() {
    // Read a disjoint partitioned file and make sure that duplicate records are not repeated
    Path indexPath = new Path(scratchPath(), "temp");
    copyDirectoryFromResources("/test_index", new File(indexPath.toString()));
    Configuration conf = sparkContext().hadoopConfiguration();
    conf.set(SpatialInputFormat.InputFormat, "envelopek(2)");
    conf.set(CSVFeatureReader.FieldSeparator, ",");
    conf.set(CSVFeatureReader.QuoteCharacters, "\"\"");
    JavaPairRDD<EnvelopeND, IFeature> records = javaSparkContext().newAPIHadoopFile(indexPath.toString(), SpatialInputFormat.class, EnvelopeND.class, IFeature.class, conf);
    List<IFeature> features = records.values().collect();
    assertEquals(3, features.size());
  }

  @FeatureReader.Metadata(shortName = "fake", filter = "*.xyz")
  public static class FakeFeatureReader extends FeatureReader {

    @Override
    public void initialize(InputSplit inputSplit, TaskAttemptContext taskAttemptContext) {

    }

    @Override
    public boolean nextKeyValue() {
      return false;
    }

    @Override
    public EnvelopeND getCurrentKey() {
      return null;
    }

    @Override
    public IFeature getCurrentValue() {
      return null;
    }

    @Override
    public float getProgress() {
      return 0;
    }

    @Override
    public void close() {

    }
  }

  public void testFileByPattern() throws IOException {
    // Create an input directory with some fake files
    Path input = new Path(scratchPath(), "temp");
    copyResource("/test_wkt.csv", new File(input.toString(), "file1.xyz"));
    copyResource("/test_wkt.csv", new File(input.toString(), "file2.xyz"));
    copyResource("/test_wkt.csv", new File(input.toString(), "file3.xz"));
    copyResource("/test_wkt.csv", new File(input.toString(), "file4.abc"));

    Configuration conf = sparkContext().hadoopConfiguration();
    conf.setClass(SpatialInputFormat.FeatureReaderClass, FakeFeatureReader.class, FeatureReader.class);
    Job job = Job.getInstance(conf);
    SpatialInputFormat.addInputPath(job, input);

    List<InputSplit> splits = new SpatialInputFormat().getSplits(job);
    assertEquals(2, splits.size());
  }
}