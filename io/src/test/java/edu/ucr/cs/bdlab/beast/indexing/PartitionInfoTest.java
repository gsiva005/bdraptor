/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.beast.indexing;

import edu.ucr.cs.bdlab.beast.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.test.JavaSparkTest;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;

public class PartitionInfoTest extends JavaSparkTest {
  public void testReadPartitions() throws IOException {
    Path inputPath = new Path(makeDirCopy("/test_index").getPath());
    FileSystem fileSystem = inputPath.getFileSystem(sparkContext().hadoopConfiguration());
    Path masterFilePath = SpatialInputFormat.getMasterFilePath(fileSystem, inputPath);
    PartitionInfo[] partitions = PartitionInfo.readPartitions(fileSystem, masterFilePath);
    assertEquals(2, partitions.length);
    assertEquals("part-00.csv", partitions[0].getFilename());
    assertEquals(2.0, partitions[0].getMaxCoord(0));
  }
}