package edu.ucr.cs.bdlab.beast.io

import edu.ucr.cs.bdlab.beast.geolite.{EnvelopeND, IFeature}
import org.apache.hadoop.mapreduce.{InputSplit, TaskAttemptContext}

@FeatureReader.Metadata(shortName = "fakescalareader", noSplit = true)
class FakeScalaReader extends FeatureReader {
  override def initialize(inputSplit: InputSplit, taskAttemptContext: TaskAttemptContext): Unit = ???

  override def nextKeyValue(): Boolean = ???

  override def getCurrentKey: EnvelopeND = ???

  override def getCurrentValue: IFeature = ???

  override def getProgress: Float = ???

  override def close(): Unit = ???
}