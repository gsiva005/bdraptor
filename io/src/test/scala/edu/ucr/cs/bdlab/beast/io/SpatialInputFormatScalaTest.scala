/*
 * Copyright 2020 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.beast.io

import org.apache.hadoop.fs.Path
import org.apache.hadoop.mapreduce.Job
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat
import org.apache.spark.test.ScalaSparkTest
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class SpatialInputFormatScalaTest extends FunSuite with ScalaSparkTest {

  test("Should work with Scala FeatureReader classes") {
    val inputFile = makeFileCopy("/features.geojson")
    var readerClass: Class[_ <: FeatureReader] = SpatialInputFormat.getFeatureReaderClass("fakescalareader")
    assert(readerClass == classOf[FakeScalaReader])

    val conf = sparkContext.hadoopConfiguration
    conf.set(SpatialInputFormat.InputFormat, "fakescalareader")
    readerClass = SpatialInputFormat.getFeatureReaderClass(conf, new Path(inputFile.getPath))
    assert(readerClass == classOf[FakeScalaReader])
  }

  test("Should respect split files") {
    val inputFile = makeFileCopy("/allfeatures.geojson")
    val readerClass: Class[_ <: FeatureReader] = SpatialInputFormat.getFeatureReaderClass("fakescalareader")
    assert(readerClass == classOf[FakeScalaReader])

    val conf = sparkContext.hadoopConfiguration
    conf.set(SpatialInputFormat.MaxSplitSize, "1024");
    conf.set(SpatialInputFormat.InputFormat, "fakescalareader")
    val job = Job.getInstance(conf)
    FileInputFormat.addInputPath(job, new Path(inputFile.getPath))
    val inputFormat = new SpatialInputFormat
    val splits = inputFormat.getSplits(job)
    assert(splits.size() == 1)
  }
}
