/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.beast.geolite;

import org.locationtech.jts.geom.Geometry;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.Externalizable;
import java.io.IOException;
import java.util.GregorianCalendar;

/**
 * A feature represents a geometry associated with zero or more fields.
 */
public interface IFeature extends Externalizable {
  // TODO make this in Scala and override parenthesis to access attributes and may be add iterators over attributes

  enum FieldType {
    /**Marker for string type (character array).*/
    StringType((short) -1),
    /**Marker for big-endian 32-bit integer type*/
    IntegerType((short) 4),
    /**Marker for big-endian 64-bit long type*/
    LongType((short) 8),
    /**Marker for big-endian 64-bit double-precision floating point type*/
    DoubleType((short) 8),
    /**A timestamp represents a date/time in milliseconds*/
    TimestampType((short) 8),
    /**A Boolean is either true or false*/
    BooleanType((short) 1);

    /**Size of this field in bytes or -1 if it is variable size*/
    public short size;
    FieldType(short s) {
      this.size = s;
    }
  }

  /**
   * The geometry contained in the feature.
   * @return the geometry in this attribute
   */
  Geometry getGeometry();

  /**
   * If this feature is mutable, this function sets the geometry in the feature.
   * It is allowed to throw an exception if this feature is immutable (constant).
   * @param geom the geometry to set in this feature
   */
  void setGeometry(Geometry geom);

  /**
   * Returns the value of the attribute at position {@code i} 0-based.
   * @param i the index of the attribute to return its value
   * @return the value of the given attribute or {@code null} if it has no value.
   */
  Object getAttributeValue(int i);

  /**
   * If names are associated with attributes, this method returns the attribute value with the given name.0
   * @param name the name of the attribute to return its value.
   * @return the value of the given attribute or {@code null} if it has no value or does not exist.
   */
  Object getAttributeValue(String name);

  /**
   * The type of the given attribute.
   * @param i the index of the attribute
   * @return the type of the attribute
   */
  FieldType getAttributeType(int i);

  /**
   * Returns the total number of attributes
   * @return the number of attributes
   */
  int getNumAttributes();

  /**
   * If names are associated with attributes, this function returns the name of the attribute at the given position
   * (0-based).
   * @param i the index of the attribute to return its name
   * @return the name of the given attribute index or {@code null} if it does not exist
   */
  String getAttributeName(int i);

  /**
   * The estimated total size of the feature in bytes including the geometry and features
   * @return the storage size in bytes
   */
  int getStorageSize();

  static boolean equals(IFeature f1, IFeature f2) {
    int numAttrs = f1.getNumAttributes();
    if (numAttrs != f2.getNumAttributes())
      return false;
    for (int iAttr = 0; iAttr < numAttrs; iAttr++) {
      String name1 = f1.getAttributeName(iAttr);
      String name2 = f2.getAttributeName(iAttr);
      boolean namesEqual = ((name1 == null || name1.length() == 0) && (name2 == null || name2.length() == 0)) ||
          (name1 != null && name2 != null && name1.equals(name2));
      if (!namesEqual)
        return false;
      if (!f1.getAttributeValue(iAttr).equals(f2.getAttributeValue(iAttr)))
        return false;
    }
    return true;
  }

  void write(DataOutput out) throws IOException;

  void readFields(DataInput in) throws IOException;

  static String toString(IFeature f) {
    StringBuilder b = new StringBuilder();
    b.append("Feature(");
    b.append(f.getGeometry() != null? f.getGeometry().toText() : "EMPTY");
    for (int $i = 0; $i < f.getNumAttributes(); $i++) {
      b.append(',');
      Object value = f.getAttributeValue($i);
      if (value instanceof GregorianCalendar)
        b.append(((GregorianCalendar)value).toZonedDateTime().toString());
      else
        b.append(value);
    }
    b.append(')');
    return b.toString();
  }
}
